package com.example.android.mvvmstart.app;

import android.content.Context;

import com.example.android.mvvmstart.data.local.user.UserDao;
import com.example.android.mvvmstart.data.local.user.UserEntity;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {UserEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract UserDao userDao();

    /**
     * Synchronized to restrict multiple threads to run instance of the database
     *
     * @param context is the context of the application
     * @return singleton instance of database
     */
    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .databaseBuilder(
                            context.getApplicationContext(),
                            AppDatabase.class,
                            "user_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
