package com.example.android.mvvmstart.data;

/**
 * This class wraps the network response and links to UI.
 *
 * @param <T> type of object being used.
 */
public class ResponseWrapper<T> {

    private boolean isSuccessful;
    private String errorMessage;
    private T responseBody;

    /**
     * Constructor
     */
    public ResponseWrapper() {
    }

    /**
     * Getters and Setters
     */
    private boolean isSuccessful() {
        return isSuccessful;
    }

    public boolean isNotSuccessful() {
        return !isSuccessful();
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(T responseBody) {
        this.responseBody = responseBody;
    }

}