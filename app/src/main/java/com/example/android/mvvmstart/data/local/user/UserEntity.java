package com.example.android.mvvmstart.data.local.user;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_table")
public class UserEntity {

    @PrimaryKey(autoGenerate = true)
    private int databaseId;

    private int id;

    private String userName;

    private String avatarUrl;

    /**
    Constructors
     */
    public UserEntity() {
    }

    public UserEntity(int id, String userName, String avatarUrl) {
        this.id = id;
        this.userName = userName;
        this.avatarUrl = avatarUrl;
    }

    /**
     Getters and Setters
     */
    public int getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(int databaseId) {
        this.databaseId = databaseId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
