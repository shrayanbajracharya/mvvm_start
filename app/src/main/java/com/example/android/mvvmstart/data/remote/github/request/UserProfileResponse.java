package com.example.android.mvvmstart.data.remote.github.request;

import com.google.gson.annotations.SerializedName;

public class UserProfileResponse {

    @SerializedName("id")
    private int userId;
    @SerializedName("avatar_url")
    private String userIcon;
    @SerializedName("login")
    private String username;
    @SerializedName("name")
    private String name;
    @SerializedName("public_repos")
    private int publicRepo;
    @SerializedName("followers")
    private int followers;
    @SerializedName("following")
    private int following;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPublicRepo() {
        return publicRepo;
    }

    public void setPublicRepo(int publicRepo) {
        this.publicRepo = publicRepo;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }
}
