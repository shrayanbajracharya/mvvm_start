package com.example.android.mvvmstart.data.remote.github.request;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @SerializedName("id")
    private int userId;
    @SerializedName("login")
    private String username;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public UserResponse(int userId, String username, String avatarUrl) {
        this.userId = userId;
        this.username = username;
        this.avatarUrl = avatarUrl;
    }

    private UserResponse(Parcel in) {
        userId = in.readInt();
        username = in.readString();
        avatarUrl = in.readString();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
