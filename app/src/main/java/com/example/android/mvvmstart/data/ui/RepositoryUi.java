package com.example.android.mvvmstart.data.ui;

public class RepositoryUi {

    // Attribtues
    private String repoName;
    private String language;

    // Constructors
    public RepositoryUi() {
    }

    public RepositoryUi(String repoName, String language) {
        this.repoName = repoName;
        this.language = language;
    }

    //Getters and Setters
    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
