package com.example.android.mvvmstart.data.ui;

import android.os.Parcel;
import android.os.Parcelable;

public class UserProfileUi implements Parcelable {

    private int userId;
    private String userIcon;
    private String username;
    private String name;
    private int followers;
    private int following;
    private int publicRepos;

    public UserProfileUi() {
    }

    public UserProfileUi(int userId, String userIcon, String username, String name,
                         int followers, int following, int publicRepos) {
        this.userId = userId;
        this.userIcon = userIcon;
        this.username = username;
        this.name = name;
        this.followers = followers;
        this.following = following;
        this.publicRepos = publicRepos;
    }

    private UserProfileUi(Parcel in) {
        userId = in.readInt();
        userIcon = in.readString();
        username = in.readString();
        name = in.readString();
        followers = in.readInt();
        following = in.readInt();
        publicRepos = in.readInt();
    }

    public static final Creator<UserProfileUi> CREATOR =
            new Creator<UserProfileUi>() {
        @Override
        public UserProfileUi createFromParcel(Parcel in) {
            return new UserProfileUi(in);
        }

        @Override
        public UserProfileUi[] newArray(int size) {
            return new UserProfileUi[size];
        }
    };

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserIcon() {
        return userIcon;
    }

    public void setUserIcon(String userIcon) {
        this.userIcon = userIcon;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userId);
        dest.writeString(userIcon);
        dest.writeString(username);
        dest.writeString(name);
        dest.writeInt(followers);
        dest.writeInt(following);
        dest.writeInt(publicRepos);
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    public int getPublicRepos() {
        return publicRepos;
    }

    public void setPublicRepos(int publicRepos) {
        this.publicRepos = publicRepos;
    }
}
