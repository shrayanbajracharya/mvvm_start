package com.example.android.mvvmstart.data.ui;

import android.os.Parcel;
import android.os.Parcelable;

public class UserUi implements Parcelable {

    private int id;
    private String userName;
    private String avatarUrl;

    public UserUi() {
    }

    public UserUi(int id, String userName, String avatarUrl) {
        this.id = id;
        this.userName = userName;
        this.avatarUrl = avatarUrl;
    }

    private UserUi(Parcel in) {
        id = in.readInt();
        userName = in.readString();
        avatarUrl = in.readString();
    }

    public static final Creator<UserUi> CREATOR = new Creator<UserUi>() {
        @Override
        public UserUi createFromParcel(Parcel in) {
            return new UserUi(in);
        }

        @Override
        public UserUi[] newArray(int size) {
            return new UserUi[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(userName);
        dest.writeString(avatarUrl);
    }
}