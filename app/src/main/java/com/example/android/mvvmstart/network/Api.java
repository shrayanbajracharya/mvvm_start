package com.example.android.mvvmstart.network;

import com.example.android.mvvmstart.data.remote.github.request.UserProfileResponse;
import com.example.android.mvvmstart.data.remote.github.request.UserResponse;
import com.example.android.mvvmstart.data.remote.github.request.repository.Repo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Api {
    @GET("users")
    Call<ArrayList<UserResponse>> getUsersList();

    @GET("users/{user}")
    Call<UserProfileResponse> getUserProfile(@Path("user") String user);

    @GET("users/{user}/repos")
    Call<ArrayList<Repo>> getPublicRepo(@Path("user") String user);
}
