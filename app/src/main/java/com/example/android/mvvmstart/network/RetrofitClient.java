package com.example.android.mvvmstart.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Api INSTANCE;
    private final String BASE_ADDRESS = "https://api.github.com/";

    public static Api getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Retrofit.Builder()
                    .baseUrl(new RetrofitClient().BASE_ADDRESS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(Api.class);
        }
        return INSTANCE;
    }
}