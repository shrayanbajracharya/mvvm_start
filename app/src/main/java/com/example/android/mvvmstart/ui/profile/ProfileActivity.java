package com.example.android.mvvmstart.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mvvmstart.R;
import com.example.android.mvvmstart.data.ResponseWrapper;
import com.example.android.mvvmstart.data.ui.UserProfileUi;
import com.example.android.mvvmstart.data.ui.UserUi;
import com.example.android.mvvmstart.ui.repository.RepoActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class ProfileActivity extends AppCompatActivity {

    ImageView userIcon;
    TextView userId;
    TextView username;
    TextView name;
    TextView followers;
    TextView following;
    TextView publicRepos;
    Button btnViewPublicRepos;

    TextView emptyStateProfile;
    ProgressBar loadingBarProfile;

    String passedUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        /*
        Setting title to actionbar
         */
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(
                    getString(R.string.profile_activity_title)
            );
        }

        /*
         Receiving data from intent
         */
        Bundle data = getIntent().getExtras();
        UserUi currentUser = new UserUi();
        if (data != null) {
            currentUser = data.getParcelable("currentUser");
        }

        /*
         Mapping id's to views
         */
        mappingViews();

        /*
        Not proceeding to fetching data if passed user data is null
         */
        if (currentUser == null)
            return;

        passedUsername = currentUser.getUserName();

            /*
              Pop up
              Searching profile through username
             */
        btnViewPublicRepos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        ProfileActivity.this, RepoActivity.class);
                intent.putExtra("username", passedUsername);
                startActivity(intent);
            }
        });

            /*
            Instantiating ProfileViewModel
            Used ProfileViewModelFactory to pass username to ProfileViewModel constructor
             */
        ProfileViewModel profileViewModel =
                ViewModelProviders.of(this,
                        new ProfileViewModelFactory(passedUsername))
                        .get(ProfileViewModel.class);

            /*
            Observer pattern to observer changes in live data of response in UserProfileResponse
             */
        profileViewModel.getUserProfileData().
                observe(this,
                        new Observer<ResponseWrapper<UserProfileUi>>() {
                            @Override
                            public void onChanged(
                                    ResponseWrapper<UserProfileUi> userProfileResponseWrapper) {

                                if (userProfileResponseWrapper.isNotSuccessful()) {

                                    hideProgressBar();
                                    showEmptyState();

                                    return;
                                }
                                UserProfileUi responseBody =
                                        userProfileResponseWrapper.getResponseBody();

                                hideProgressBar();
                                displayData(responseBody);
                            }
                        });

        profileViewModel.fetchProfileDataFromNetwork();


    }

    /**
     * Mapping Views of UserProfileResponse Activity
     */
    private void mappingViews() {
        userIcon = findViewById(R.id.profile_user_icon);
        userId = findViewById(R.id.profile_user_id);
        username = findViewById(R.id.profile_username);
        name = findViewById(R.id.profile_name);
        followers = findViewById(R.id.profile_followers);
        following = findViewById(R.id.profile_following);
        publicRepos = findViewById(R.id.profile_public_repos);
        btnViewPublicRepos = findViewById(R.id.btn_view_public_repos);
    }

    public void hideProgressBar() {
        loadingBarProfile = findViewById(R.id.loading_bar_profile);
        loadingBarProfile.setVisibility(View.INVISIBLE);
    }

    /**
     * Displays text when there is no data to be displayed in UserProfileResponse Activity
     */
    private void showEmptyState() {
        emptyStateProfile = findViewById(R.id.empty_state_profile);

        String emptyStateText =
                String.format("No user with username %s found",
                        passedUsername);
        emptyStateProfile.setText(emptyStateText);
    }

    /**
     * Displays data in respective views
     * @param userProfileUi stores essential attributes to be shown to views
     */
    public void displayData(UserProfileUi userProfileUi) {

        // Formatting strings
        String stringId = "UserResponse ID: " + userProfileUi.getUserId();
        String stringUsername = String.format("Username: %s", userProfileUi.getUsername());
        String stName = String.format("Name: %s", userProfileUi.getName());
        String stFollowers = "Followers: " + userProfileUi.getFollowers();
        String stFollowing = "Following: " + userProfileUi.getFollowing();
        String stPublicRepos = "Public Repos: " + userProfileUi.getPublicRepos();

        // Displaying in the views
        Glide.with(getApplicationContext())
                .load(userProfileUi.getUserIcon())
                .into(userIcon);
        userId.setText(stringId);
        username.setText(stringUsername);
        name.setText(stName);
        followers.setText(stFollowers);
        following.setText(stFollowing);
        publicRepos.setText(stPublicRepos);
        btnViewPublicRepos.setVisibility(View.VISIBLE);
    }

}
