package com.example.android.mvvmstart.ui.profile;

import com.example.android.mvvmstart.data.ResponseWrapper;
import com.example.android.mvvmstart.data.remote.github.request.UserProfileResponse;
import com.example.android.mvvmstart.data.ui.UserProfileUi;
import com.example.android.mvvmstart.network.Api;
import com.example.android.mvvmstart.network.RetrofitClient;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class ProfileViewModel extends ViewModel {

    /**
     * Call is used to send request to server to fetch UserProfileResponse data
     * Call<UserProfileResponse> means response will be in type UserProfileResponse (object)
     *
     * MutableLiveData is used to observe changes in the data when any changes are seen
     * MutableLiveData<ResponseWrapper<UserProfileUi>> means
     * it can post value of type ResponseWrapper<UserProfileUi>
     */
    private Call<UserProfileResponse> call;
    private MutableLiveData<ResponseWrapper<UserProfileUi>> userProfileLiveDataUi;

    /**
     * Constructor
     * @param username is the passed username whose profile info needs to be fetched
     */
    ProfileViewModel(String username) {
        init(username);
    }

    /**
     * Initialization of variables call and userProfileLiveDataUi
     * @param username is the passed username whose profile info needs to be fetched
     */
    private void init(String username) {
        Api api = RetrofitClient.getInstance();
        call = api.getUserProfile(username);
        userProfileLiveDataUi = new MutableLiveData<>();
    }

    /**
     * Fetches data from network by requesting to server
     */
    void fetchProfileDataFromNetwork() {
        final ResponseWrapper<UserProfileUi> wrapper = new ResponseWrapper<>();
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserProfileResponse> call,
                                   @NonNull Response<UserProfileResponse> response) {
                wrapper.setSuccessful(response.isSuccessful());

                // If response is not successful, directly return
                if (wrapper.isNotSuccessful()) {
                    wrapper.setErrorMessage(
                            String.format("Server has sent %s", response.message()));
                    userProfileLiveDataUi.postValue(wrapper);
                    return;
                }

                UserProfileResponse responseBody = response.body();
                if (responseBody == null){
                    wrapper.setErrorMessage("Received null instead of actual data");
                    userProfileLiveDataUi.postValue(wrapper);
                    return;
                }

                UserProfileUi userProfileUi = transformResponseToUi(responseBody);

                wrapper.setErrorMessage("Successfully fetched data");
                wrapper.setResponseBody(userProfileUi);
                userProfileLiveDataUi.postValue(wrapper);
            }

            @Override
            public void onFailure(@NonNull Call<UserProfileResponse> call,
                                  @NonNull Throwable t) {
                wrapper.setSuccessful(false);
                wrapper.setErrorMessage(t.getLocalizedMessage());
            }
        });
    }

    /**
     * Used to get UserProfileData (UserProfileUi)
     * @return userProfileLiveDataUi so that it can be received in Ui
     */
    MutableLiveData<ResponseWrapper<UserProfileUi>> getUserProfileData() {
        return userProfileLiveDataUi;
    }

    /**
     *
     * @param userProfileResponse consists of userProfileResponse information from remote
     * @return userProfileResponse information for Ui
     */
    private UserProfileUi transformResponseToUi(UserProfileResponse userProfileResponse) {
        UserProfileUi result = new UserProfileUi();

        if (userProfileResponse != null) {
            result.setUserId(userProfileResponse.getUserId());
            result.setUsername(userProfileResponse.getUsername());
            result.setUserIcon(userProfileResponse.getUserIcon());
            result.setName(userProfileResponse.getName());
            result.setFollowers(userProfileResponse.getFollowers());
            result.setFollowing(userProfileResponse.getFollowing());
            result.setPublicRepos(userProfileResponse.getPublicRepo());
        }

        return result;
    }

}
