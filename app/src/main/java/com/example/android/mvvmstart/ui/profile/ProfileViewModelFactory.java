package com.example.android.mvvmstart.ui.profile;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ProfileViewModelFactory implements ViewModelProvider.Factory {

    private String passedUsername;

    ProfileViewModelFactory(String passedUsername) {
        this.passedUsername = passedUsername;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ProfileViewModel(passedUsername);
    }
}
