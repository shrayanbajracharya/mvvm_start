package com.example.android.mvvmstart.ui.repository;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.android.mvvmstart.R;
import com.example.android.mvvmstart.data.ResponseWrapper;
import com.example.android.mvvmstart.data.ui.RepositoryUi;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RepoActivity extends AppCompatActivity {

    RecyclerView repoRecyclerView;
    RepoAdapter repoAdapter = new RepoAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo);

        /*
          Setting title to ActionBar of RepoActivity
         */
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(
                    getString(R.string.repo_activity_title)
            );
        }

        /*
          instantiating RecyclerView of RepoActivity
         */
        repoRecyclerView = findViewById(R.id.repo_recycler_view);
        repoRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        repoRecyclerView.setHasFixedSize(true);

        repoRecyclerView.setAdapter(repoAdapter);

        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        /*
          Instantiating RepoViewModel and fetching data according to the passed username
         */
        String passedUsername = intent.getStringExtra("username");

        RepoViewModel repoViewModel =
                ViewModelProviders
                        .of(this, new RepoViewModelFactory(passedUsername))
                        .get(RepoViewModel.class);

        /*
          Observing LiveData from repoViewModel
         */
        repoViewModel
                .getRepoLiveData()
                .observe(this, new Observer<ResponseWrapper<ArrayList<RepositoryUi>>>() {

            @Override
            public void onChanged(
                    ResponseWrapper<ArrayList<RepositoryUi>> arrayListResponseWrapper) {

                if (arrayListResponseWrapper.isNotSuccessful()){
                    Toast.makeText(RepoActivity.this,
                                    "Failures retrieving repo list data",
                                    Toast.LENGTH_SHORT).show();

                    return;
                }

                ArrayList<RepositoryUi> responseBody = arrayListResponseWrapper.getResponseBody();
                
                if (responseBody == null){
                    Toast.makeText(RepoActivity.this,
                            "Empty repo list received from server",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                /*
                Adding new data to Adapter of RepoActivity to be shown in RecyclerView
                 */
                repoAdapter.add(responseBody);

            }
        });

        /*
        Used to fetch repo list from network
         */
        repoViewModel.fetchRepoListFromNetwork();

    }
}
