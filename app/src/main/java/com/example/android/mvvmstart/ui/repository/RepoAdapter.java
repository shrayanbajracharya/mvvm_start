package com.example.android.mvvmstart.ui.repository;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.mvvmstart.R;
import com.example.android.mvvmstart.data.ui.RepositoryUi;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoViewHolder> {

    /**
     * List to be displayed in RecyclerView
     */
    private ArrayList<RepositoryUi> repositoryUiList;

    /**
     * Constructors
     */
    RepoAdapter() {
        repositoryUiList = new ArrayList<>();
    }

    public RepoAdapter(ArrayList<RepositoryUi> repositoryUiList) {
        this.repositoryUiList = repositoryUiList;
    }

    /**
     *
     * @param data is the new data that are to be added to be shown
     */
    void add(ArrayList<RepositoryUi> data) {
        if (data == null){
            return;
        }

        repositoryUiList.addAll(data);
        notifyItemRangeInserted(repositoryUiList.size(), data.size());

    }

    @NonNull
    @Override
    public RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_repo_list, parent, false);
        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoViewHolder holder, int position) {

        RepositoryUi currentRepositoryUi = repositoryUiList.get(position);

        holder.repoCount.setText(String.valueOf(position + 1));
        holder.repoName.setText(currentRepositoryUi.getRepoName());
        holder.language.setText(currentRepositoryUi.getLanguage());

    }

    @Override
    public int getItemCount() {
        return repositoryUiList.size();
    }

    class RepoViewHolder extends RecyclerView.ViewHolder{

        TextView repoCount;
        TextView repoName;
        TextView language;

        RepoViewHolder(@NonNull View itemView) {
            super(itemView);

            repoCount = itemView.findViewById(R.id.repo_count);
            repoName = itemView.findViewById(R.id.repo_name);
            language = itemView.findViewById(R.id.language);

        }
    }

}
