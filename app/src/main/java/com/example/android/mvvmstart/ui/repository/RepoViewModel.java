package com.example.android.mvvmstart.ui.repository;

import com.example.android.mvvmstart.data.ResponseWrapper;
import com.example.android.mvvmstart.data.remote.github.request.repository.Repo;
import com.example.android.mvvmstart.data.ui.RepositoryUi;
import com.example.android.mvvmstart.network.Api;
import com.example.android.mvvmstart.network.RetrofitClient;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class RepoViewModel extends ViewModel {

    /**
     * Call is used to make request, response is in type ArrayList<Repo>
     *     MutableLiveData is used to send value to Ui
     */
    private Call<ArrayList<Repo>> call;
    private MutableLiveData<ResponseWrapper<ArrayList<RepositoryUi>>> repoLiveData;

    /**
     * Constructor
     * @param passedUsername is the username passed to ViewModel
     *                       whose Repo List are to be shown
     */
    RepoViewModel(String passedUsername){
        init(passedUsername);
    }

    /**
     * Initialization for fetching Repo List of user
     * @param passedUsername is is the username passed to ViewModel
     *                       whose Repo List are to be shown
     */
    private void init(String passedUsername){
        Api apiInstance = RetrofitClient.getInstance();
        call = apiInstance.getPublicRepo(passedUsername);

        repoLiveData = new MutableLiveData<>();
    }

    /**
     * Fetches repo list from network
     */
    void fetchRepoListFromNetwork(){

        final ResponseWrapper<ArrayList<RepositoryUi>> repoUiResponseWrapper = new ResponseWrapper<>();

        call.enqueue(new Callback<ArrayList<Repo>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Repo>> call,
                                   @NonNull Response<ArrayList<Repo>> response) {
                repoUiResponseWrapper.setSuccessful(response.isSuccessful());

                if (repoUiResponseWrapper.isNotSuccessful()){
                    // TODO: Set EmptyState for Repo List
                    repoUiResponseWrapper.setErrorMessage("No result (repo) found");
                    return;
                }

                ArrayList<Repo> responseBody = response.body();

                if (responseBody == null){
                    repoUiResponseWrapper.setErrorMessage("Response Body is null");
                    return;
                }

                ArrayList<RepositoryUi> repositoryUiList = transformResponseToRepoUi(responseBody);
                repoUiResponseWrapper.setResponseBody(repositoryUiList);
                repoLiveData.postValue(repoUiResponseWrapper);

            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Repo>> call,
                                  @NonNull Throwable t) {
                repoUiResponseWrapper.setSuccessful(false);
                repoUiResponseWrapper.setErrorMessage("Bad Response");
                repoLiveData.postValue(repoUiResponseWrapper);
            }
        });
    }

    /**
     * Transforming remote data to ui data
     * @param responseBody is remote data to be transformed
     * @return result is ui data after transformation
     */
    private ArrayList<RepositoryUi> transformResponseToRepoUi(ArrayList<Repo> responseBody) {

        ArrayList<RepositoryUi> result = new ArrayList<>();

        for (Repo currentRepo: responseBody
             ) {

            RepositoryUi currentRepositoryUi = new RepositoryUi();
            currentRepositoryUi.setRepoName(currentRepo.getName());
            currentRepositoryUi.setLanguage(currentRepo.getLanguage());

            result.add(currentRepositoryUi);
        }
        return result;
    }

    /**
     *
     * @return repo list LiveData
     */
    MutableLiveData<ResponseWrapper<ArrayList<RepositoryUi>>> getRepoLiveData() {
        return repoLiveData;
    }

}
