package com.example.android.mvvmstart.ui.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class RepoViewModelFactory implements ViewModelProvider.Factory {

    /**
     * Username passed for fetching Repo List
     */
    private String passedUsername;

    /**
     * Constructor
     * @param passedUsername is the username passed from UserProfileResponse Activity
     */
    RepoViewModelFactory(String passedUsername) {
        this.passedUsername = passedUsername;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new RepoViewModel(passedUsername);
    }
}
