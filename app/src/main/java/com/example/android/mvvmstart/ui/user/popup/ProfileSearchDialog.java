package com.example.android.mvvmstart.ui.user.popup;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.android.mvvmstart.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class ProfileSearchDialog extends DialogFragment {

    /**
     * Input field of popup search
     */
    private EditText inputUsername;

    /**
     * Listener for sending input username to UserProfileResponse Activity
     */
    private popupSearchInputListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        /*
          Instantiating Popup DialogFragment
         */
        View view = inflater.inflate(R.layout.fragment_search_user, container, false);

        TextView actionCancel = view.findViewById(R.id.action_cancel);
        TextView actionSearch = view.findViewById(R.id.action_search);
        inputUsername = view.findViewById(R.id.input_username);

        getDialog().setTitle("Search by username");

        /*
          Cancel action
         */
        actionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        /*
          Search action
         */
        actionSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String usernameFromDialog = inputUsername.getText().toString().trim();

                if (TextUtils.isEmpty(usernameFromDialog)){
                    inputUsername.setError("Please enter username");
                }else {
                    listener.retrieveDialogData(usernameFromDialog);
                    getDialog().dismiss();
                }

            }
        });

        return view;
    }

    /**
     * For handling ClassCastException when passing data from Fragment to Activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (popupSearchInputListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + "must implement popupSearchInputListener");
        }

    }

    /**
     * Best Practice to pass data from Fragment to Activity
     */
    public interface popupSearchInputListener {
        void retrieveDialogData(String usernameFromDialog);
    }
}
