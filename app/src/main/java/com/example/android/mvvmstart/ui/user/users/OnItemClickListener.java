package com.example.android.mvvmstart.ui.user.users;

import com.example.android.mvvmstart.data.ui.UserUi;

public interface OnItemClickListener {
    void onItemClicked(UserUi currentUser);
}
