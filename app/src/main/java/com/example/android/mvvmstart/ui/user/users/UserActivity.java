package com.example.android.mvvmstart.ui.user.users;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.mvvmstart.R;
import com.example.android.mvvmstart.data.ResponseWrapper;
import com.example.android.mvvmstart.data.ui.UserUi;
import com.example.android.mvvmstart.ui.profile.ProfileActivity;
import com.example.android.mvvmstart.ui.user.popup.ProfileSearchDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class UserActivity extends AppCompatActivity
        implements OnItemClickListener, ProfileSearchDialog.popupSearchInputListener {

    private ProgressBar loadingBar;
    private TextView emptyState;
    private FloatingActionButton fabSearch;
    private RecyclerView recyclerView;
    private UserViewModel viewModel;

    private OnItemClickListener listener = new OnItemClickListener() {
        @Override
        public void onItemClicked(UserUi currentUser) {
            Intent intent = new Intent(UserActivity.this, ProfileActivity.class);
            intent.putExtra("currentUser", currentUser);
            startActivity(intent);
        }
    };

    private UserAdapter userAdapter = new UserAdapter(listener);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.main_activity_title));
        }

        if (isNotNetworkAvailable()) {
            hideProgressBar();
            showEmptyState();
        }

        mappingViews();
        initRecyclerView();
        initViewModel();
        observeUsers();

        fabSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        viewModel.fetchUsersListFromNetwork();
        Log.v("UserActivity: ", "Data fetched from network");

    }

    void mappingViews() {
        fabSearch = findViewById(R.id.fab_search);
        loadingBar = findViewById(R.id.loadingBar);
        emptyState = findViewById(R.id.empty_state);
        recyclerView = findViewById(R.id.user_list);
    }

    void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(userAdapter);
    }

    void initViewModel() {
        viewModel = ViewModelProviders.of(this).get(UserViewModel.class);
    }

    void observeUsers() {
        /* It is better to use observer before fetching data from network as it will be faster
        while setting in adapter as compared to fetching data from network and then waiting to
        setting adapter and displaying/load data */

        viewModel.getUsers().observe(this, new Observer<ResponseWrapper<ArrayList<UserUi>>>() {
            @Override
            public void onChanged(ResponseWrapper<ArrayList<UserUi>> usersResponseWrapper) {
                if (usersResponseWrapper.isNotSuccessful()) {
                    Toast.makeText(
                            UserActivity.this,
                            usersResponseWrapper.getErrorMessage(),
                            Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                hideProgressBar();
                userAdapter.add(usersResponseWrapper.getResponseBody());
            }
        });
    }

    private void openDialog() {
        ProfileSearchDialog popupDialog = new ProfileSearchDialog();
        popupDialog.show(getSupportFragmentManager(), "UserProfileResponse Search");
    }

    @Override
    public void onItemClicked(UserUi currentUser) {
        Toast.makeText(this, "Item Clicked", Toast.LENGTH_SHORT).show();
    }

    private boolean isNotNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return !(activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    public void hideProgressBar() {
        loadingBar.setVisibility(View.GONE);
    }

    public void showEmptyState() {
        emptyState.setVisibility(View.VISIBLE);
        emptyState.setText(getString(R.string.no_internet_connection));
    }

    @Override
    public void retrieveDialogData(String usernameFromDialog) {
        UserUi currentUser = new UserUi();
        currentUser.setUserName(usernameFromDialog);

        Intent intent = new Intent(UserActivity.this, ProfileActivity.class);
        intent.putExtra("currentUser", currentUser);
        startActivity(intent);
    }
}
