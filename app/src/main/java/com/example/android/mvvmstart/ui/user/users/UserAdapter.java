package com.example.android.mvvmstart.ui.user.users;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mvvmstart.R;
import com.example.android.mvvmstart.data.ui.UserUi;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    /**
     * mUsersList is the List of users to be displayed
     * listener is used to listen to clicks for getting clicked user
     */
    private ArrayList<UserUi> mUsersList;
    private OnItemClickListener listener;
    private Context context;

    private InternalClickListener internalClickListener = new InternalClickListener() {

        @Override
        public void onVhClicked(int position) {

            Log.d(TAG, "onVhClicked: " + position);

            final UserUi clickedUser = mUsersList.get(position);
            listener.onItemClicked(clickedUser);
        }

    };
    private int noPosition = RecyclerView.NO_POSITION;

    /**
     * Constructors
     *
     * @param listener is to listen to clicked user's position
     */
    UserAdapter(OnItemClickListener listener) {
        mUsersList = new ArrayList<>();
        this.listener = listener;
    }

    public UserAdapter(ArrayList<UserUi> usersList, OnItemClickListener listener) {
        mUsersList = usersList;
        this.listener = listener;
    }

    /**
     * Adds new users data to list
     *
     * @param users is the new uses data
     */
    void add(List<UserUi> users) {

        if (users.isEmpty()) return;

        mUsersList.addAll(users);
        notifyItemRangeInserted(mUsersList.size(), users.size());
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View customView = inflater.inflate(R.layout.item_user_list, parent, false);
        return new UserViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        final UserUi currentUser = mUsersList.get(position);

        Log.d(TAG, "onBindViewHolder: is called");

        holder.internalClickListener = internalClickListener;

        String stId = "UserResponse ID: " + currentUser.getId();
        holder.userId.setText(stId);
        holder.username.setText(
                String.format("Username: %s", currentUser.getUserName())
        );

        Glide.with(context)
                .load(currentUser.getAvatarUrl())
                .into(holder.userIcon);

    }

    @Override
    public int getItemCount() {
        return mUsersList.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {

        /**
         * Views in the custom_list inflated to RecyclerView
         */
        ImageView userIcon;
        TextView userId;
        TextView username;
        InternalClickListener internalClickListener;

        UserViewHolder(@NonNull View itemView) {
            super(itemView);
            userIcon = itemView.findViewById(R.id.user_icon);
            userId = itemView.findViewById(R.id.user_id);
            username = itemView.findViewById(R.id.username);

            itemView.setOnClickListener(new View.OnClickListener() {

                int position = getAdapterPosition();

                @Override
                public void onClick(View v) {
                    if (internalClickListener != null
                            && position != RecyclerView.NO_POSITION) {

                        internalClickListener.onVhClicked(position);
                    }
                }
            });

        }

    }

    private interface InternalClickListener {
        void onVhClicked(int position);
    }

}
