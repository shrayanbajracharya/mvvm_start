package com.example.android.mvvmstart.ui.user.users;

import android.app.Application;
import android.os.AsyncTask;

import com.example.android.mvvmstart.app.AppDatabase;
import com.example.android.mvvmstart.data.local.user.UserDao;
import com.example.android.mvvmstart.data.local.user.UserEntity;

import java.util.List;

import androidx.lifecycle.LiveData;

public class UserRepository {

    private UserDao userDao;
    private LiveData<List<UserEntity>> allUsers;

    public UserRepository(Application application) {
        AppDatabase database
                = AppDatabase.getInstance(application.getApplicationContext());
        userDao = database.userDao();
        allUsers = userDao.getAllUsers();
    }

    /**
     * AsyncTask operations
     */
    public void insert(UserEntity userEntity){
        new InsertUserAsyncTask(userDao).execute(userEntity);
    }

    public static class InsertUserAsyncTask extends AsyncTask<UserEntity, Void, Void>{

        UserDao userDao;

        private InsertUserAsyncTask(UserDao userDao){
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(UserEntity... userEntities) {
            userDao.insert(userEntities[0]);
            return null;
        }
    }

    public void update(UserEntity userEntity){
        new UpdateUserAsyncTask(userDao).execute(userEntity);
    }

    public static class UpdateUserAsyncTask extends AsyncTask<UserEntity, Void, Void>{

        UserDao userDao;

        private UpdateUserAsyncTask(UserDao userDao){
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(UserEntity... userEntities) {
            userDao.update(userEntities[0]);
            return null;
        }
    }

    public void delete(UserEntity userEntity){
        new UpdateUserAsyncTask(userDao).execute(userEntity);
    }

    public static class DeleteUserAsyncTask extends AsyncTask<UserEntity, Void, Void>{

        UserDao userDao;

        private DeleteUserAsyncTask(UserDao userDao){
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(UserEntity... userEntities) {
            userDao.delete(userEntities[0]);
            return null;
        }
    }

    public void deleteAllUsers(){
        new UpdateUserAsyncTask(userDao).execute();
    }

    public static class DeleteAllUsersAsyncTask extends AsyncTask<Void, Void, Void>{

        UserDao userDao;

        private DeleteAllUsersAsyncTask(UserDao userDao){
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            userDao.deleteAllUsers();
            return null;
        }
    }

    public LiveData<List<UserEntity>> getAllUsers(){
        return allUsers;
    }

}
