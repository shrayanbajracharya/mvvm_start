package com.example.android.mvvmstart.ui.user.users;

import android.util.Log;

import com.example.android.mvvmstart.data.ResponseWrapper;
import com.example.android.mvvmstart.data.local.user.UserEntity;
import com.example.android.mvvmstart.data.remote.github.request.UserResponse;
import com.example.android.mvvmstart.data.ui.UserUi;
import com.example.android.mvvmstart.network.Api;
import com.example.android.mvvmstart.network.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserViewModel extends ViewModel {

    /**
     * Response from server
     * Call is used to send request and get response in type ArrayList<UserResponse>
     * MutableLiveData is used to send value to Ui
     */

    private Call<ArrayList<UserResponse>> call;
    private MutableLiveData<ResponseWrapper<ArrayList<UserUi>>> userLiveDataUi;

    /**
     * Local storage
     */
    private UserRepository repository;
    private LiveData<List<UserEntity>> liveDataLocalUser;

    /**
     * Constructor
     */
    public UserViewModel() {
        init();
    }

    /**
     * Initialization of UserViewModel
     */
    private void init() {
        Api api = RetrofitClient.getInstance();
        call = api.getUsersList();
        userLiveDataUi = new MutableLiveData<>();
    }

    /**
     * Fetches Users List from network
     */
    void fetchUsersListFromNetwork() {

        final ResponseWrapper<ArrayList<UserUi>> wrapper = new ResponseWrapper<>();

        call.enqueue(new Callback<ArrayList<UserResponse>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<UserResponse>> call,
                                   @NonNull Response<ArrayList<UserResponse>> response) {

                wrapper.setSuccessful(response.isSuccessful());

                // If response is not successful, directly return
                if (!response.isSuccessful()) {
                    Log.v("UserListViewModel", "Error fetching data from network");

                    wrapper.setErrorMessage(String.format(
                            "server has sent %s", response.message()));
                    userLiveDataUi.postValue(wrapper);
                    return;
                }

                ArrayList<UserResponse> responseBody = response.body();
                if (responseBody == null) {
                    wrapper.setErrorMessage("Received null instead of actual data");
                    userLiveDataUi.postValue(wrapper);
                    return;
                }

                ArrayList<UserUi> userUis = transformResponseToUiData(responseBody);

                wrapper.setErrorMessage("Successfully fetched data.");
                wrapper.setResponseBody(userUis);


                userLiveDataUi.postValue(wrapper);
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<UserResponse>> call, @NonNull Throwable t) {
                Log.v("Main Activity: ", "Bad Response");

                wrapper.setSuccessful(false);
                wrapper.setErrorMessage(t.getLocalizedMessage());

                userLiveDataUi.postValue(wrapper);
            }
        });
    }

    /**
     * Transforming remote UserResponse to ui UserResponse
     *
     * @param userResponses is remote UserResponse to be transformed
     * @return result in ui UserResponse after transformation
     */
    private ArrayList<UserUi> transformResponseToUiData(ArrayList<UserResponse> userResponses) {
        ArrayList<UserUi> result = new ArrayList<>();

        for (UserResponse userResponse :
                userResponses) {

            UserUi userUi = new UserUi();
            userUi.setId(userResponse.getUserId());
            userUi.setAvatarUrl(userResponse.getAvatarUrl());
            userUi.setUserName(userResponse.getUsername());

            result.add(userUi);

        }

        return result;
    }

    /**
     * Used to get MutableLiveData
     *
     * @return MutableLiveData with ui Users response body
     */
    MutableLiveData<ResponseWrapper<ArrayList<UserUi>>> getUsers() {
        Log.v("getUsers:", "UserResponse data returned");
        return userLiveDataUi;
    }

}
